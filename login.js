var errorUsername;
var errorpwsd;
var errorConfirmacion;
var errorEmail;
$(document).ready(loginText);

/**
 * Carga el texto del login
 * 
 */
function loginText(){
    $('#wrapper').html(""+
    "<img class='logoLogin'></img>" +
    "<div id='frameLogin' class = 'container'><form action='' method='GET'><div><p><input type='text' placeholder = 'Usuario o E-mail' name = 'username' class='textLoginInputs' required>"+
    "</p></div>" +
    "<div><p><input type='password' name = 'pswd' class='textLoginInputs' placeholder = 'Contraseña' required></p></div>" +
    "<div><p><input type='button' id = 'login' value='login!' class = 'buttonInputs'></p></div></form><p>" +
    "<div id = 'linksLogin' ><a href= 'javascript:void(0)' id = 'changePswd'>¿No recuerdas tu Contraseña? Pulsa aquí</a></p><a href= 'javascript:void(0)' id = 'registerUser'>¿Aún no estás registrado? Hazlo ahora</a></div></div>");
    $('#registerUser').click(registerText);
    $('#changePswd').click(cpAskEmailText);

}

/**
 * Carga la página de registro
 */
function registerText() {
    $("#frameLogin").replaceWith('<i class="fa fa-arrow-left lastpage"></i><div id="frameRegister" class = "container"><form action="" method="">'+
        '<div class = "regContain"><p><input type="text" placeholder = "E-mail" id  = "email" name = "E-mail" class="textLoginInputs inputRegister" required></p>'+
        '<p><span class="error_form" id="emailErrorMessage"></span></p></div>'+
        '<div class = "regContain"><p><input type="text" placeholder = "Usuario" id = "username" name = "username" class="textLoginInputs inputRegister" required></p>'+
        '<p><span class="error_form" id="eusernameErrorMessage"></span></p></div>'+
        '<div class = "regContain"><p><input type="password" id = "contraseña" placeholder = "Contraseña" name = "passwd" class="textLoginInputs inputRegister" required></p>'+
        '<p><span class="error_form" id="epasswdErrorMessage"></span></p></div>'+
        '<div class = "regContain"><p><input type="password" placeholder = "Confirmación" id = "confirmacion" name = "confirmacion" class="textLoginInputs inputRegister" required></p>'+
        '<p><span class="error_form" id="erepetirErrorMessage"></span></p></div>'+ 
        '<input type="button" id = "register" value="REGISTRARSE" class = "buttonLoginInputs"></form></div>');
        $(".lastpage").click(loginText);
        $('#register').click(function(){
            errorUsername = checkUsername();
            errorEmail = checkEmail();
            errorpwsd = checkPswd();
            errorConfirmacion = checkConfirmacion();
            if (!errorEmail && !errorUsername && !errorpwsd && !errorConfirmacion ){
                registerEndText();
            }
        });
}
/**
 * Página final al registrarse un usuario
 */
function registerEndText(){
    $("#frameRegister").replaceWith('<div class = "textParagraphLogin"> Registro realizado correctamente<br/> Por favor,comprueba tu E-mail y<br/>'+
    'pulsa en el link para condirmar tu<br/>registro.</div><p><input type="button" id = "back" value="Volver" class = "buttonLoginInputs"></p></div>');
    $('#back').click(loginText);
}
/**
 * Carga la página para recuperar la contraseña
 */
function cpAskEmailText(){
    $("#frameLogin").replaceWith('<i class="fa fa-arrow-left lastpage"></i><div id = "askEmailFrame"><form action="" method="GET"><div class = "textParagraphLogin">'+
            'Por favor,introduce tu nombre de<br/>'+
            'usuario o tu dirección de correo<br/>'+
            'electrónico. Recibirás por correo <br/>'+
            'electrónico un enlace para crear<br/>'+
            'una nueva conbtraseña.</div>'+
            '<div class = "regContain"><p><input type="text" placeholder = "E-mail" id  = "email" name = "E-mail" class="textLoginInputs inputRegister" required></p>'+
            '<p><span class="error_form" id="emailErrorMessage"></span></p></div>'+
        '<p><input type="button" id = "send" value="Enviar" class = "buttonLoginInputs"></p></form></div>');
        $(".lastpage").click(loginText);
        $('#send').click(function(){
            var emailCorrecto = checkEmail();
            if (!emailCorrecto){
                cpSendEmailText();
            }
        });
}
/**
 * Carga la página al introducir un correo
 */
function cpSendEmailText(){
    $("#askEmailFrame").replaceWith('<div id = "frameAffCpEmail"><div class = "textParagraphLogin">'+
        'Se ha enviado un correo<br/>'+
        'electronico a su cuenta.Haga click<br/>'+
        'en el enlace que le hemos enviado<br/>'+
        'para establecer la nueva.'+
    '</div><p><input type="button" id = "back" value="Volver" class = "buttonLoginInputs"></p></div>');
    $('#back').click(loginText);
}