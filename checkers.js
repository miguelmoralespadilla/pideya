/**
 * Comprueba si el email es correcto
 */
function checkEmail() {
    var pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailText =  $('#email').val();
    if (pattern.test(emailText) && emailText !== ''){
        $("#emailErrorMessage").hide();
        $("#email").css("border-bottom","2px solid #34F458");
        return false;
    }else{
        $("#emailErrorMessage").html("El campo no tiene el formato correcto.");
        $("#emailErrorMessage").show();
        $("#email").css("border-bottom","2px solid #F90A0A");
        return true;
    }
}

/**
 * Comprueba si la caja del usuario esta vacia o tiene el minimo de caracteres
 */
function checkUsername() {
        var username_length = $("#username").val().length;
        var username = $("#username").val();
        if (username !== '') {
           $("#eusernameErrorMessage").hide();
           $("#username").css("border-bottom","2px solid #34F458");
           return false;
        } else if (username_length<3){
            $("#eusernameErrorMessage").html("Mínimo 3 caracteres.");
            $("#eusernameErrorMessage").show();
            $("#username").css("border-bottom","2px solid #F90A0A");
            return true;
        }
        else {
           $("#eusernameErrorMessage").html("El campo está vacio.");
           $("#eusernameErrorMessage").show();
           $("#username").css("border-bottom","2px solid #F90A0A");
           return true;
        }
     }

     /**
      * Comprueba si la contraseña tiene un minimo de caracteres
      */
function checkPswd() {
        var password_length = $("#contraseña").val().length;
        if (password_length < 8) {
           $("#epasswdErrorMessage").html("Mínimo 8 caracteres");
           $("#epasswdErrorMessage").show();
           $("#contraseña").css("border-bottom","2px solid #F90A0A");
           return true;
        } else {
           $("#epasswdErrorMessage").hide();
           $("#contraseña").css("border-bottom","2px solid #34F458");
           return false;
        }
     }

     /**
      * Comprueba si la contraseña es igual a la anterior
      */
function checkConfirmacion() {
        var password = $("#contraseña").val().length;
        var retype_password = $("#confirmacion").val();
        if (password !== retype_password && password < 8 ) {
           $("#erepetirErrorMessage").html(" No es igual a la contraseña.");
           $("#erepetirErrorMessage").show();
           $("#confirmacion").css("border-bottom","2px solid #F90A0A");
           return true;
        } else {
           $("#erepetirErrorMessage").hide();
           $("#confirmacion").css("border-bottom","2px solid #34F458");
           return false;
        }
     }